import {BrowserRouter, Route, Routes} from "react-router-dom";
import logo from './logo.svg';
import './App.css';
import Login from './pages/Login';
import Signin from './pages/Signin';
import Home from './pages/Home';
import Info from './pages/Info';
import HomeProfesseur from './pages/HomeProfesseur';

function App() {
  
  return (
    
    <div className="App">
      <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"></link>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/Login" element={<Login />} />
          <Route path="/Signin" element={<Signin />} />
          <Route path="/Home" element={<Home />} />
          <Route path="/HomeProfesseur" element={<HomeProfesseur />} />
          <Route path="/Info" element={<Info />} />
        </Routes>
      </BrowserRouter>       
    </div>
  );
}

export default App;
