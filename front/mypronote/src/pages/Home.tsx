import React,{ useState, useEffect } from 'react';
import Tableau from '../component/Tableau';
import Header from '../component/Header';
import { useLocation,useNavigate } from 'react-router-dom';
import './Home.css';

function Home(){
    let navigate = useNavigate();
    let location = useLocation();
    interface note{
        id:number;
        id_eleve:number;
        matiere: string;
        date: Date;
        note: number;
        coef: number;
        libelle: string;
        id_prof:string;
    }
    interface matiere{
        id:number;
        libelle:string;
        eu:string;
    }
    const state:any= location.state;

    const [matieres, setMatieres] = useState<matiere[]>([]);
    const [prenom, setPrenom] = useState<string>();
    const [nom, setNom] = useState<string>();
    const [role, setRole] = useState<string>();
    const [id, setId] = useState<string>();
    const [login, setLogin] = useState<string>();
    
    useEffect(()=>{
        fetch(`http://localhost/projet_web/api/requetes/get_matieres`)
        .then((reponse) => reponse.json())
        .then((resultat) =>{
            console.log(resultat)
            setMatieres(resultat)
            console.log("matieres "+matieres)
        });
    },[])
   
   
    if(state === null){
        navigate('/login')
    }
    else{
        if(state.login === null){
            navigate('/login')
        }

        fetch(`http://localhost/projet_web/api/requetes/get_utilisateur?login="${state.login}"`)
        .then((reponse) => reponse.json())
        .then((resultat) =>{
            setPrenom(resultat.prenom)
            setNom(resultat.nom)
            setRole(resultat.role)
            setId(resultat.id)
            setLogin(resultat.login)
        });
        return (
            <div>
                <Header login={login} role={role}/>
                <div className='HomePage'>
                    <div className="Info_user">{role} : {prenom} {nom}</div>
                    <div className='Notes'>
                        {matieres.map((matiere:matiere)=>{
                            return(
                                <Tableau matiere={matiere.libelle} id={id}></Tableau>
                            )
                        })}
                    </div>
                </div>
            </div>
        );
    }
    return(
        <div></div>
    )
   
}

export default Home;