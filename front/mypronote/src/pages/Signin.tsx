import React, { useState } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import Header from '../component/Header';
import './Signin.css';

function Signin(){
    let navigate = useNavigate();
   

    const [login, setLogin] = useState<string>();
    const [prenom, setPrenom] = useState<string>();
    const [nom, setNom] = useState<string>();
    const [role, setRole] = useState<string>();
    const [password, setPassword] = useState<string>();

    const changeLogin = (e: any) =>{
        setLogin(e.target.value)
    }
    const changePrenom = (e: any) =>{
        setPrenom(e.target.value)
    }
    const changeNom = (e: any) =>{
        setNom(e.target.value)
    }
    const changeRole = (e: any) =>{
        setRole(e.target.value)
    }
    const changePassword = (e: any) =>{
        setPassword(e.target.value)
    }

    const goLogin = () =>{
        navigate("/login")
    }
    const ajoutUtilisateur = () => {
        fetch(`http://localhost/projet_web/api/requetes/post_utilisateur`,{
            mode: 'no-cors',
            method: 'POST',
            body: JSON.stringify({
                login: login,
                prenom: prenom,
                nom: nom,
                role:"Eleve",
                mdp: password
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
        navigate("/login")
       
    }
    return (
        <div>
            <div className="Header">
                <div className='name'>My Pronote</div>
            </div>
            <div className='InputsCadre'>
                <div className='InputsSign'>
                    <div className='Username'>
                        <input type="text" id="username" name="username" placeholder="Username" onChange={changeLogin} />
                    </div>
                    <div className='Username'>
                        <input type="text" id="Prenom" name="Prenom" placeholder="Prenom" onChange={changePrenom} />                   
                    </div>
                    <div className='Username'>
                        <input type="text" id="Nom" name="Nom" placeholder="Nom" onChange={changeNom} />
                    </div >
                    <div className='Username'>
                        <input type="password" id="pass" name="password" placeholder="Password" onChange={changePassword} />
                    </div>
                    <button onClick={ajoutUtilisateur}>S'enregistrer</button>
                    <button value="Connexion" className='Connexion' onClick={goLogin}>Se connecter</button> 
                </div>
            </div>
        </div>
    );
}

export default Signin;