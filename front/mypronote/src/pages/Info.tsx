import React, { useState, useEffect } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import Header from '../component/Header';
import logo_edit from '../images/editer.svg';
import './Info.css';

function Info(){
    interface style{
        display:string;
    }
    let navigate = useNavigate();
    let location = useLocation();
    let state:any = location.state;
    const [id, setId] = useState<number>();
    const [login, setLogin] = useState<string>();
    const [prenom, setPrenom] = useState<string>();
    const [nom, setNom] = useState<string>();
    const [logintmp, setLogintmp] = useState<string>();
    const [prenomtmp, setPrenomtmp] = useState<string>();
    const [nomtmp, setNomtmp] = useState<string>();
    const [role, setRole] = useState<string>();
    const [styleDivLogin,setStyleDivLogin] = useState<style>({display:"none"})
    const [styleDivPrenom,setStyleDivPrenom] = useState<style>({display:"none"})
    const [styleDivNom,setStyleDivNom] = useState<style>({display:"none"})
    const [login_dep, setLogin_dep] = useState<string>();
    const [prenom_dep, setPrenom_dep] = useState<string>();
    const [nom_dep, setNom_dep] = useState<string>();

    const changeLogin = () =>{
        setLogin(logintmp)
    }
    const changePrenom = () =>{
        setPrenom(prenomtmp)
    }
    const changeNom = () =>{
        console.log(nomtmp)
        setNom(nomtmp)
    }

    const changeLogintmp = (e: any) =>{
        setLogintmp(e.target.value)
    }
    const changePrenomtmp = (e: any) =>{
        setPrenomtmp(e.target.value)
    }
    const changeNomtmp = (e: any) =>{
        console.log(e.target.value)
        setNomtmp(e.target.value)
        console.log(nomtmp)
    }

    const annuler = ()=>{
        setLogin(login_dep)
        setPrenom(prenom_dep)
        setNom(nom_dep)
        setLogintmp(login_dep)
        setPrenomtmp(prenom_dep)
        setNomtmp(nom_dep)
        setStyleDivLogin({display:"none"})
        setStyleDivPrenom({display:"none"})
        setStyleDivNom({display:"none"})
    }
    const afficheDivLogin = () =>{
        if(styleDivLogin.display === "none")
            setStyleDivLogin({display:"block"})
        else
            setStyleDivLogin({display:"none"})
    }
    const afficheDivPrenom = () =>{
        if(styleDivPrenom.display === "none")
            setStyleDivPrenom({display:"block"})
        else
            setStyleDivPrenom({display:"none"})
    }
    const afficheDivNom = () =>{
        if(styleDivNom.display === "none")
            setStyleDivNom({display:"block"})
        else
            setStyleDivNom({display:"none"})
    }
    const sendChange = () =>{
        fetch(`http://localhost/projet_web/api/requetes/update_utilisateur`,{
            method: 'POST',
            mode:'no-cors',
            body: JSON.stringify({
                id: id,
                login: login,
                nom: nom,
                prenom: prenom
            })
        })
        console.log(role)
        if(role === "Eleve"){
            navigate("/home",{
                state:{
                    login: login
                }
            });
        }else if(role === "Professeur"){
            navigate("/homeprofesseur",{
                state:{
                    login: login
                }
            });
        }
    }
   useEffect(()=>{
    fetch(`http://localhost/projet_web/api/requetes/get_utilisateur?login="${state.login}"`)
        .then((reponse) => reponse.json())
        .then((resultat) =>{
            setId(resultat.id)
            setLogin(resultat.login)
            setPrenom(resultat.prenom)
            setNom(resultat.nom)
            setRole(resultat.role)
            setLogintmp(resultat.login)
            setPrenomtmp(resultat.prenom)
            setNomtmp(resultat.nom)
            setLogin_dep(resultat.login)
            setPrenom_dep(resultat.prenom)
            setNom_dep(resultat.nom)
        });

   },[])
    if(state === null){
        navigate("/login")
    }else{
        if(state.login === undefined){
            navigate('/login')
        }

        
        return (
            <div>
                <Header login={login} role={role}/>
                <div className="Infos">
                    <div className='Input_info'>
                        <div>
                            <div>Login : {login} <img src={logo_edit} alt='edit' height="20px" width="20px" className="logo_edit" onClick={afficheDivLogin}></img></div>
                            <div style={styleDivLogin}>
                                <input type="text" id="username" name="username" placeholder="Username"  onChange={changeLogintmp}/>
                                <button onClick={changeLogin}>Valider</button>
                            </div>                       
                        </div>
                        <div>
                            <div>Prenom : {prenom} <img src={logo_edit} alt='edit' height="20px" width="20px" className="logo_edit" onClick={afficheDivPrenom}></img></div>
                            <div style={styleDivPrenom}>
                                <input type="text" id="Prenom" name="Prenom" placeholder="Prenom"  onChange={changePrenomtmp}/>
                                <button onClick={changePrenom}>Valider</button>
                            </div>                                
                        </div>
                        <div>
                            <div>Nom : {nom} <img src={logo_edit} alt='edit' height="20px" width="20px" className="logo_edit" onClick={afficheDivNom}></img></div>
                            <div style={styleDivNom}>
                                <input type="text" id="Nom" name="Nom" placeholder="Nom" onChange={changeNomtmp}/>
                                <button onClick={changeNom}>Valider</button>
                            </div>                   
                        </div>
                        <div>
                            <div>Role : {role}</div>
                            
                        </div>
                        <div className='Bouttons'>
                            <button onClick={sendChange}>Enregistrer</button> 
                            <button onClick={annuler}>Annuler</button> 
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    return(
        <div></div>
    )
   
}

export default Info;