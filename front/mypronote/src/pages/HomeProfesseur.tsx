import { prependOnceListener } from 'process';
import React,{ useState, useEffect } from 'react';
import Header from '../component/Header';
import { useLocation,useNavigate } from 'react-router-dom';
import './HomeProfesseur.css';

function HomeProfesseur(){
    interface note{
        id:number;
        id_eleve:number;
        matiere: string;
        date: Date;
        note: number;
        coef: number;
        libelle: string;
        id_prof:string;
    }
    interface eleve{
        id:number;
        login:string;
        prenom:string;
        nom:string;
    }
    interface matiere{
        id:number;
        libelle:string;
        eu:string;
    }

    let navigate = useNavigate();
    let location = useLocation();
    const state:any= location.state;
    
    const [matieres, setMatieres] = useState<matiere[]>([]);
    const [matiere, setMatiere] = useState<matiere>();
    const [eleves, setEleves] = useState<eleve[]>([]);
    const [eleve, setEleve] = useState<eleve>();
    const [note,setNote] = useState<string>();
    const [coef,setCoef] = useState<string>();
    const [libelle,setLibelle] = useState<string>();
    const [date,setDate] = useState<Date>();
    const [prenom, setPrenom] = useState<string>();
    const [nom, setNom] = useState<string>();
    const [role, setRole] = useState<string>();
    const [id, setId] = useState<string>();
    const [login, setLogin] = useState<string>();
    const [notes, setNotes] = useState<note[]>([]);

    useEffect(()=>{
        fetch(`http://localhost/projet_web/api/requetes/get_matieres`)
        .then((reponse) => reponse.json())
        .then((resultat) =>{
            setMatieres(resultat)
        });
    },[])
   
    useEffect(()=>{
        fetch(`http://localhost/projet_web/api/requetes/get_eleves`)
        .then((reponse) => reponse.json())
        .then((resultat) =>{
            setEleves(resultat)
        });
    },[])

    useEffect(()=>{
        fetch(`http://localhost/projet_web/api/requetes/get_note_from_prof?id=${id}`)
        .then((reponse) => reponse.json())
        .then((resultat) =>{
            setNotes(resultat)
        });
    },[id])

    const ajoutNote = () => {
        console.log(note)
        fetch(`http://localhost/projet_web/api/requetes/post_note`,{
            mode: 'no-cors',
            method: 'POST',
            body: JSON.stringify({
                matiere: matiere?.libelle,
                id_eleve: eleve?.id,
                note: note,
                coef:coef,
                libelle: libelle,
                date:date,
                id_prof:id
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
       
    }

    const clickNote = () =>{
        
    }
    if(state === null){
        navigate('/login')
    }
    else{
        if(state.login === null){
            navigate('/login')
        }
        fetch(`http://localhost/projet_web/api/requetes/get_utilisateur?login="${state.login}"`)
        .then((reponse) => reponse.json())
        .then((resultat) =>{
            setPrenom(resultat.prenom)
            setNom(resultat.nom)
            setRole(resultat.role)
            setId(resultat.id)
            setLogin(resultat.login)
        });
        fetch(`http://localhost/projet_web/api/requetes/get_note_from_prof?id=${id}`)
        .then((reponse) => reponse.json())
        .then((resultat) =>{
            setNotes(resultat)
        });
        return(
            <div>
                <Header login={state.login} role={role}/>
                <div className='HomePage'>
                    <div className="Info_user">{role} : {prenom} {nom}</div>
                    <div className='Ajout_note'>
                        <div className='Ajout_note_label'>Ajouter une note :</div>
                        <div className='Input_note'>
                            <div className="w3-dropdown-hover">
                                <button className="w3-button">Elève : {eleve?.prenom} {eleve?.nom}</button>
                                <div className="w3-dropdown-content w3-bar-block w3-card-4">
                                    {eleves.map((eleve:eleve)=>{
                                        return(
                                            <div  className="w3-bar-item w3-button" onClick={()=> {setEleve(eleve)}}>{eleve.prenom} {eleve.nom}</div>
                                        )
                                    })}
                                </div>
                            </div>
                            <div className="w3-dropdown-hover">
                                <button className="w3-button">Matière : {matiere?.libelle}</button>
                                <div className="w3-dropdown-content w3-bar-block w3-card-4">
                                    {matieres.map((matiere:matiere)=>{
                                        return(
                                            <div  className="w3-bar-item w3-button" onClick={()=> {setMatiere(matiere)}}>{matiere.libelle}</div>
                                        )
                                    })}
                                </div>
                            </div>
                            <label> Note </label>
                            <input className="" type="number"  onChange={(e:any)=>{setNote(e.target.value)}} min="0" max="20"></input>
                            <label> Coefficient </label>
                            <input className="" type="number"  onChange={(e:any)=>{setCoef(e.target.value)}} min="1" max="5"></input>
                            <label> Intitullé </label>
                            <input className="" type="text" onChange={(e:any)=>{setLibelle(e.target.value)}}></input>
                            <label> Date </label>
                            <input className="Date" type="date" onChange={(e:any)=>{setDate(e.target.value)}}></input>
                            <button className="w3-button w3-round-xlarge" onClick={ajoutNote}>Ajouter la note</button>
                        </div>
                    </div>
                    <div className='Note_prof'>
                        <table  className="w3-table w3-striped w3-bordered">
                            <thead className='Infos_tab'>
                                Mes notes
                            </thead>                
                                {notes.map((note: note) =>{
                                    return (
                                        <tr className='Ligne_note' onClick={clickNote}>
                                            <div className='Note_eleve_prof'>{eleves.find((eleve:eleve)=>{if(eleve.id === note.id_eleve) return eleve})?.prenom} {eleves.find((eleve:eleve)=>{if(eleve.id === note.id_eleve) return eleve})?.nom}</div>
                                            <div className='Note_note_matiere'>{note.matiere} </div>
                                            <div className='Note_note_prof'>{note.note}/20 </div>                                            
                                            <div className='Note_libelle_prof'>{note.libelle} </div>
                                            <div className='Note_date_prof'> du {note.date}</div>                                                                         
                                        </tr>
                                        )
                                    })
                                }          
                        </table>
                    </div>
                </div>
            </div>
        )
    }
    return(
        <div></div>
    )
}

export default HomeProfesseur;