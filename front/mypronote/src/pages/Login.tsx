import React, { useState } from 'react';
import { useNavigate   } from 'react-router-dom';
import {  } from 'react-router'
import Header from '../component/Header';
import './Login.css';

function Login({history}:any){
    interface style{
        display:string
    }

    let navigate = useNavigate();

    const [login, setLogin] = useState<string>();
    const [password, setPassword] = useState<string>();

    const changeLogin = (e: any) =>{
        setLogin(e.target.value)
    }

    const changePassword = (e: any) =>{
        setPassword(e.target.value)
    }

    const goSignin = () => {
        navigate("/signin")
    }

    const [erreurStyle,setErreurStyle] = useState<style>({display:'none'});

    const afficheErreur = () =>{
        setErreurStyle({display:'block'})
    }
    const verifUtilisateur = () => {
        fetch(`http://localhost/projet_web/api/requetes/verif_utilisateur`,{
            method: 'PUT',
            body: JSON.stringify({
                login: login,
                mdp: password
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
        .then((reponse) => reponse.json())
        .then((resultat) =>{
            if(resultat.reponse === "true"){
                fetch(`http://localhost/projet_web/api/requetes/get_utilisateur?login="${login}"`)
                .then((reponse) => reponse.json())
                .then((resultat) =>{
                    if(resultat.role === "Eleve"){
                        navigate("/home",{
                            state:{
                                login: resultat.login
                            }
                        });
                    }else if(resultat.role === "Professeur"){
                        navigate("/homeprofesseur",{
                            state:{
                                login: resultat.login
                            }
                        });
                    }
                               
                });              
            }else{
                afficheErreur()
            }
        });       
    }

    
    return (
        <div>
            <div className="Header">
                <div className='name'>My Pronote</div>
            </div>
            <div className='Page'>
                <div className='Inputs'>
                    <div>
                        <input type="text" className='Login' placeholder="Username" onChange={changeLogin}/>
                    </div>

                    <div>
                        <input type="password" className='Mdp' placeholder="Password" onChange={changePassword} />
                    </div>         
                    <button value="Connexion" className='Connexion' onClick={verifUtilisateur}>Connexion</button>
                    <button value="Enregistrer" className='Enregistrer' onClick={goSignin}>S'enregistrer</button>
                    <div className='Erreur' style={erreurStyle}>Mot de passe ou login incorrect</div>
                </div>
            </div>
        </div>
    );
}

export default Login;