import { connect } from 'http2';
import { useState, useEffect  } from 'react';
import './Tableau.css';


function Tableau(props:any){
    interface note{
        id:number;
        id_eleve:number;
        matiere: string;
        date: Date;
        note: number;
        coef: number;
        libelle: string;
        
    }
    const [notes, setNotes] = useState<note[]>([]);

    useEffect(()=>{
        fetch(`http://localhost/projet_web/api/requetes/get_note_from_matiere?matiere=${props.matiere}&id_eleve=${props.id}`,{
            method:'GET'
        })
        .then((reponse) =>
        reponse.json()
        )
        .then((resultat) =>{
            setNotes(resultat)
        });
    },[props.matiere,props.id])

    return (
        <div>
            <table  className="w3-table w3-striped w3-bordered">
                <thead className='Infos_tab'>
                    {props.matiere}
                </thead>                
                    {notes.map((note: note) =>{
                        return (
                            <tr className='Ligne_note'>
                                <div className='Note_libelle'>{note.libelle} </div>
                                <div className='Note_date'> du {note.date}</div>   
                                <div className='Note_note'>{note.note}/20 </div>
                                                            
                            </tr>
                            )
                        })
                    }          
            </table>
        </div>
    );
}
export default Tableau;