import React, { useState } from 'react';
import './Header.css';
import { Link  } from 'react-router-dom';
import logo_maison from '../images/maison.svg';
import logo_traits from '../images/traits_header.svg';
import { useNavigate  } from 'react-router-dom';

function Header(props:any){
    interface style{
        display:string
    }
    let navigate = useNavigate();
    const goLogin = () => {
        navigate("/login")
    }

    const goHome = () =>{
        if(props.role === "Eleve"){
            navigate("/home",{
                state:{
                    login: props.login
                }
            });
        }else if(props.role === "Professeur"){
            navigate("/homeprofesseur",{
                state:{
                    login: props.login
                }
            });
        }
    }

    const goInfo = () => {
        navigate("/info",{
            state:{
                login:props.login
            }
        })
    }
    const [optionStyle,setOptionStyle] = useState<style>({display:'none'});

    const clickOptions = () =>{
        if(optionStyle.display === 'none'){
            setOptionStyle({display:'block'})
        }else{
            setOptionStyle({display:'none'})
        }
    }
    return(
        <div className="Header">
            <img src={logo_maison} alt='home' height="40px" width="40px" className="logo_maison" onClick={goHome}></img>               
            <div className='name'>My Pronote</div>
            <img src={logo_traits} onClick={clickOptions} alt='autre' height="40px" width="40px" className="logo_traits"></img>
            <div className='options'  style={optionStyle}>
                <div className='info'  onClick={goInfo} >Informations</div>
                <div className='deco' onClick={goLogin} >Deconnexion</div>               
            </div>
        </div>
    )
}

export default Header;