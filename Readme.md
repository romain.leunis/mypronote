# Projet web 
****
	S1 ENSIIE
****

### Front :
	React/TypeScript
	W3-CSS
****
### Back :
	API Rest
	PHP
	WampServer64
****

## Besoin pour utilisation :
	npm
	WampServer64
	BDD SQL 
****
## Mise en place :
	Créer la bdd dans phpMyAdmin sur Wamp
	Placer le dossier Api dans Wamp ( dossier www )
	Lancer Wamp
	$ npm start (Dans le dossier myPronote)
