<?php
    header("Access-Control-Allow-Origin: *");

    // Format des données envoyées
    header("Content-Type: application/json; charset=UTF-8");
    
    // Méthode autorisée
    header("Access-Control-Allow-Methods: PUT");
    
    // Durée de vie de la requête
    header("Access-Control-Max-Age: 3600");
    
    // Entêtes autorisées
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    if($_SERVER['REQUEST_METHOD'] == 'PUT'){


        include_once '../config/database.php';
        include_once '../models/utilisateur.php';

        $database = new Database();
        $db = $database->getConnection();

        $utilisateur = new Utilisateur($db);

        $donnees = json_decode(file_get_contents("php://input"));      

        if(!empty($donnees->login) && !empty($donnees->mdp)){
            $utilisateur->login = $donnees->login;
            $utilisateur->mdp = $donnees->mdp;

            $reponse = $utilisateur->connection_utilisateur();

            if($reponse->fetch()['COUNT(*)'] > 0){
                echo json_encode(["reponse" => "true"]);
                http_response_code(200);
                
            }else if($reponse->fetch()['COUNT(*)'] == 0){
                echo json_encode(["reponse" => "false"]);
                http_response_code(200);
                
            }else{
                http_response_code(503);
            }      
        }
        else{
            http_response_code(405);
            echo json_encode(["message" => "La méthode n'est pas autorisée"]);
        }
    }
?>