<?php
    header("Access-Control-Allow-Origin: *");

    // Format des données envoyées
    header("Content-Type: application/json; charset=UTF-8");
    
    // Méthode autorisée
    header("Access-Control-Allow-Methods: DELETE");
    
    // Durée de vie de la requête
    header("Access-Control-Max-Age: 3600");
    
    // Entêtes autorisées
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    if($_SERVER['REQUEST_METHOD'] == 'DELETE'){


        include_once '../config/database.php';
        include_once '../models/note.php';

        $database = new Database();
        $db = $database->getConnection();

        $note = new Note($db);
        $donnees = json_decode(file_get_contents("php://input"));

        if(!empty($donnees->id)){
            $note->id = $donnees->id;

            if($note->suprimer()){
                http_response_code(201);
            }else{
                http_response_code(503);
            }
        }else{
            http_response_code(401);
        }
        

        
    }
    else{
        http_response_code(405);
        echo json_encode(["message" => "La méthode n'est pas autorisée"]);
    }
?>