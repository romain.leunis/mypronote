<?php
    header("Access-Control-Allow-Origin: http://localhost:3000");

    // Format des données envoyées
    header("Content-Type: application/json; charset=UTF-8");
    
    // Méthode autorisée
    header("Access-Control-Allow-Methods: GET");
    
    // Durée de vie de la requête
    header("Access-Control-Max-Age: 3600");
    
    // Entêtes autorisées
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    if($_SERVER['REQUEST_METHOD'] == 'GET'){


        include_once '../config/database.php';
        include_once '../models/utilisateur.php';

        $database = new Database();
        $db = $database->getConnection();

        $utilisateur = new Utilisateur($db);

        

        if(isset($_GET['login'])){
            $login = $_GET['login'];
            $reponse = $utilisateur->lire_utilisateur($login);
        }else{
            $reponse = $utilisateur->lire();
        }
        
        if($reponse->rowCount() > 0){
            $data = $reponse->fetch();
        // On initialise un tableau associatif
            $myutilisateur = [
                "id"=>$data['id'],
                "login"=>$data['Login'],
                "nom"=>$data['Nom'],
                "prenom"=>$data['Prenom'],
                "role"=>$data['Role']
            ];

            http_response_code(200);

            // On encode en json et on envoie
            echo json_encode($myutilisateur);
        }else{
             http_response_code(401);
        }       
    }
    else{
        http_response_code(405);
        echo json_encode(["message" => "La méthode n'est pas autorisée"]);
    }
?>