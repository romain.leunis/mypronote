<?php
    header("Access-Control-Allow-Origin: *");

    // Format des données envoyées
    header("Content-Type: application/json; charset=UTF-8");
    
    // Méthode autorisée
    header("Access-Control-Allow-Methods: POST");
    
    // Durée de vie de la requête
    header("Access-Control-Max-Age: 3600");
    
    // Entêtes autorisées
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    if($_SERVER['REQUEST_METHOD'] == 'POST'){


        include_once '../config/database.php';
        include_once '../models/utilisateur.php';

        $database = new Database();
        $db = $database->getConnection();

        $utilisateur = new Utilisateur($db);

        $donnees = json_decode(file_get_contents("php://input"));
        
        if(!empty($donnees->login)){
            $utilisateur->login = $donnees->login;
            $utilisateur->nom = $donnees->nom;
            $utilisateur->prenom = $donnees->prenom;
            $utilisateur->mdp = $donnees->mdp;
            $utilisateur->role = $donnees->role;

            if($utilisateur->ajouter()){
                http_response_code(200);
                echo json_encode(["message" => "L'ajout a été effectué"]);
            }else{
                http_response_code(503);
            }
           
        }else{
             http_response_code(401);
        }      
              
    }
    else{
        http_response_code(405);
        echo json_encode(["message" => "La méthode n'est pas autorisée"]);
    }
?>