<?php
    header("Access-Control-Allow-Origin: *");

    // Format des données envoyées
    header("Content-Type: application/json; charset=UTF-8");
    
    // Méthode autorisée
    header("Access-Control-Allow-Methods: GET");
    
    // Durée de vie de la requête
    header("Access-Control-Max-Age: 3600");
    
    // Entêtes autorisées
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    if($_SERVER['REQUEST_METHOD'] == 'GET'){


        include_once '../config/database.php';
        include_once '../models/note.php';

        $database = new Database();
        $db = $database->getConnection();

        $note = new Note($db);
   

        if(isset($_GET['matiere']) && isset($_GET['id_eleve'])){
            $note->matiere = $_GET['matiere'];
            $note->id_eleve = $_GET['id_eleve'];
            $reponse = $note->lire_matiere_eleve();
            if($reponse->rowCount() > 0){
                $tableauNotes = [];

                while($row = $reponse->fetch(PDO::FETCH_ASSOC)){
                    extract($row);

                    $n = [
                        "id" => $id,
                        "id_eleve" => $id_eleve,
                        "note" => $note,
                        "matiere" => $matiere,
                        "coef" => $coef,
                        "date" => $date,
                        "libelle" => $libelle
                    ];

                    $tableauNotes[] = $n;
                }
                http_response_code(200);
                echo json_encode($tableauNotes);
            }  
        }           
    }
    else{
        http_response_code(405);
        echo json_encode(["message" => "La méthode n'est pas autorisée"]);
    }
?>