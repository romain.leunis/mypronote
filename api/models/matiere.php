 <?php
 class Matiere {
        private $connexion;
        private $table = "matiere";

        public $id;
        public $libelle;
        public $ue;

        public function __construct($db){
            $this->connexion = $db;
        }   

        public function lire(){

            $sql = "SELECT * FROM " . $this->table;
            
            $requete = $this->connexion->prepare($sql);

            $requete->execute();

            return $requete;
        }

    }
?>