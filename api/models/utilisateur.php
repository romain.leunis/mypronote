<?php
	class Utilisateur{
		private $connexion;
        private $table = "utilisateur";

        public $id;
        public $login;
        public $nom;
        public $prenom;
        public $mdp;
        public $role;

         public function __construct($db){
            $this->connexion = $db;
        }   
        
        public function lire(){

            $sql = "SELECT * FROM " . $this->table;
            
            $requete = $this->connexion->prepare($sql);

            $requete->execute();

            return $requete;
        }

        public function lire_utilisateur($login){

            $sql = "SELECT * FROM " . $this->table . " WHERE Login=" . $login;           
            
            $requete = $this->connexion->prepare($sql);

            $requete->execute();

            return $requete;
        }

        public function lire_eleves(){

            $sql = "SELECT * FROM " . $this->table . " WHERE Role='Eleve'";           
            
            $requete = $this->connexion->prepare($sql);

            $requete->execute();

            return $requete;
        }

        public function ajouter(){

            $sql = "INSERT INTO " . $this->table . " SET Login=:login, Nom=:nom, Prenom=:prenom, Mdp=:mdp, Role=:role";

            $requete = $this->connexion->prepare($sql);

            $this->login=htmlspecialchars(strip_tags($this->login));
            $this->nom=htmlspecialchars(strip_tags($this->nom));
            $this->prenom=htmlspecialchars(strip_tags($this->prenom));
            $this->mdp=htmlspecialchars(strip_tags($this->mdp));
            $this->role=htmlspecialchars(strip_tags($this->role));

            $requete->bindParam(":login", $this->login);
            $requete->bindParam(":nom", $this->nom);
            $requete->bindParam(":prenom", $this->prenom);
            $requete->bindParam(":mdp", $this->mdp);
            $requete->bindParam(":role", $this->role);

            if($requete->execute()){
                return true;
            }
            return false;

        }

        public function suprimer(){
            $sql = "DELETE FROM " . $this->table . " WHERE id=:id";

            $requete = $this->connexion->prepare($sql);

            $this->id=htmlspecialchars(strip_tags($this->id));

            $requete->bindParam(":id", $this->id);

            if($requete->execute()){
                return true;
            }
            return false;
        }

        public function connection_utilisateur(){
            $sql = "SELECT COUNT(*) FROM " . $this->table . " WHERE Login=:login AND Mdp=:mdp";

            $requete = $this->connexion->prepare($sql);

            $this->login=htmlspecialchars(strip_tags($this->login));
            $this->mdp=htmlspecialchars(strip_tags($this->mdp));

            $requete->bindParam(":login", $this->login);
            $requete->bindParam(":mdp", $this->mdp);

            if($requete->execute()){
            	return $requete;
            }
            return null;
        }

        public function modifier(){
            $sql = "UPDATE " . $this->table . " SET Login=:login, Nom=:nom, Prenom=:prenom WHERE id=:id";

            $requete = $this->connexion->prepare($sql);

            $this->id=htmlspecialchars(strip_tags($this->id));
            $this->nom=htmlspecialchars(strip_tags($this->nom));
            $this->login=htmlspecialchars(strip_tags($this->login));
            $this->prenom=htmlspecialchars(strip_tags($this->prenom));

            $requete->bindParam(":id", $this->id);
            $requete->bindParam(":nom", $this->nom);
            $requete->bindParam(":login", $this->login);
            $requete->bindParam(":prenom", $this->prenom);

            if($requete->execute()){
                return true;
            }
            return false;

        }
	}
?>