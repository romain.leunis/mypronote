<?php
    class Note {
        private $connexion;
        private $table = "notes";

        public $id_eleve;
        public $note;
        public $matiere;
        public $libelle;
        public $coef;
        public $date;
        public $id_prof;

        public function __construct($db){
            $this->connexion = $db;
        }   

        public function lire(){

            $sql = "SELECT * FROM " . $this->table;
            
            $requete = $this->connexion->prepare($sql);

            $requete->execute();

            return $requete;
        }

        public function lire_eleve($id){

            $sql = "SELECT * FROM " . $this->table . " WHERE id_eleve=" . $id;           
            
            $requete = $this->connexion->prepare($sql);

            $requete->execute();

            return $requete;
        }

        public function lire_matiere_eleve(){

            $sql = "SELECT * FROM " . $this->table . " WHERE id_eleve=:id_eleve AND matiere=:matiere";           
            
            $requete = $this->connexion->prepare($sql);

            $this->id_eleve=htmlspecialchars(strip_tags($this->id_eleve));
            $this->matiere=htmlspecialchars(strip_tags($this->matiere));

            $requete->bindParam(":id_eleve", $this->id_eleve);
            $requete->bindParam(":matiere", $this->matiere);

            $requete->execute();

            return $requete;
        }

        public function lire_prof(){

            $sql = "SELECT * FROM " . $this->table . " WHERE id_prof=:id_prof";           
            
            $requete = $this->connexion->prepare($sql);

            $this->id_prof=htmlspecialchars(strip_tags($this->id_prof));

            $requete->bindParam(":id_prof", $this->id_prof);

            $requete->execute();

            return $requete;
        }

        public function ajouter(){

            $sql = "INSERT INTO " . $this->table . " SET id_eleve=:id_eleve, note=:note, matiere=:matiere, coef=:coef, date=:date, libelle=:libelle, id_prof=:id_prof";

            $requete = $this->connexion->prepare($sql);

            $this->id_eleve=htmlspecialchars(strip_tags($this->id_eleve));
            $this->note=htmlspecialchars(strip_tags($this->note));
            $this->matiere=htmlspecialchars(strip_tags($this->matiere));
            $this->coef=htmlspecialchars(strip_tags($this->coef));
            $this->date=htmlspecialchars(strip_tags($this->date));
            $this->libelle=htmlspecialchars(strip_tags($this->libelle));
            $this->id_prof=htmlspecialchars(strip_tags($this->id_prof));

            $requete->bindParam(":id_eleve", $this->id_eleve);
            $requete->bindParam(":note", $this->note);
            $requete->bindParam(":matiere", $this->matiere);
            $requete->bindParam(":coef", $this->coef);
            $requete->bindParam(":date", $this->date);
            $requete->bindParam(":libelle", $this->libelle);
            $requete->bindParam(":id_prof", $this->id_prof);

            if($requete->execute()){
                return true;
            }
            return false;

        }

        public function modifier(){
            $sql = "UPDATE " . $this->table . " SET note=:note, matiere=:matiere, coef=:coef, date=:date, libelle=:libelle WHERE id=:id";

            $requete = $this->connexion->prepare($sql);

            $this->id=htmlspecialchars(strip_tags($this->id));
            $this->note=htmlspecialchars(strip_tags($this->note));
            $this->matiere=htmlspecialchars(strip_tags($this->matiere));
            $this->coef=htmlspecialchars(strip_tags($this->coef));
            $this->date=htmlspecialchars(strip_tags($this->date));
            $this->libelle=htmlspecialchars(strip_tags($this->libelle));

            $requete->bindParam(":id", $this->id);
            $requete->bindParam(":note", $this->note);
            $requete->bindParam(":matiere", $this->matiere);
            $requete->bindParam(":coef", $this->coef);
            $requete->bindParam(":date", $this->date);
            $requete->bindParam(":libelle", $this->libelle);

            if($requete->execute()){
                return true;
            }
            return false;

        }

        public function suprimer(){
            $sql = "DELETE FROM " . $this->table . " WHERE id=:id";

            $requete = $this->connexion->prepare($sql);

            $this->id=htmlspecialchars(strip_tags($this->id));

            $requete->bindParam(":id", $this->id);

            if($requete->execute()){
                return true;
            }
            return false;
        }

    }
?>